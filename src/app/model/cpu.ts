// This represents our data model
export interface CPU {
  id:number;
  name:string;
  desc:string;
  cores:number;
  speed:number;
  bus:number;
  aux:number;
  lat:number;
  lng:number;
}
