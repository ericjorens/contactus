import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { CPU } from '../model/cpu';

// This pipe gets the aux name from the observable array
@Pipe({
  name: 'cpuAux',
})
export class CpuAuxPipe implements PipeTransform {

  // Our data intolerance value
  private readonly REJECTED_IDX = 4;
  private readonly REJECTED_VAL = "REJECTED!";

  transform(cpu$?: Observable<CPU[]>, aux?: number): string {
    let auxName = '';
    if (!!cpu$ && !!aux) {
      cpu$.subscribe((cpus: CPU[]) => {
        //get the aux cpu based on the cpu id in the array
        cpus.forEach((c:CPU)=>{
          // if the id matches then return the cpu name
          if(c.id === aux){
            auxName = c.name
          } else if (aux === this.REJECTED_IDX) {
            // otherwise return the intolerance value
            auxName = this.REJECTED_VAL;
          }
        })
      });
    }
    return auxName;
  }
}
