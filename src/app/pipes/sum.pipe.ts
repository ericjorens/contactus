import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generates the cpu rank by muliplying the cores and speed
 * applying both a baseline and threshold
 */
@Pipe({
  name: 'cpuRank'
})
export class SumPipe implements PipeTransform {
  // transform our data to adhere to a baseline
  private readonly SUM_BASELINE = 40;

  // limit our data to a threshold
  private readonly SUM_THRESHOLD = 100;

  transform(cores: number, speed:number): number {
    let val  = (cores * speed) + this.SUM_BASELINE;
    if (val > this.SUM_THRESHOLD){
      return this.SUM_THRESHOLD; // upper limit to our translated data
    }
    return val;
  }

}
