import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

// spotter widget
import { CpuSpotterComponent, CardBodyComponent, CardPaneComponent, CpuTableComponent, MapRowComponent } from './components/cpu-spotter';
import { SortColDirective } from './directives/sort-col.directive';
import { SumPipe } from './pipes/sum.pipe';
import { PointDirective } from './directives/point.directive';
import { CpuAuxPipe } from './pipes/cpu-aux.pipe';
import { TemplateDrivenFormComponent } from './components/template-driven-form/template-driven-form.component';
import { ReactiveFormComponent } from './components/reactive-form/reactive-form.component';
import { ForbiddenValidatorDirective } from './directives/forbidden-validator.directive';



declare var google: any;

@NgModule({
  declarations: [
    AppComponent,
    CardPaneComponent,
    CardBodyComponent,
    MapRowComponent,
    CpuTableComponent,
    CpuSpotterComponent,
    SortColDirective,
    SumPipe,
    PointDirective,
    CpuAuxPipe,
    TemplateDrivenFormComponent,
    ReactiveFormComponent,
    ForbiddenValidatorDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBjhYC8FyaT8zdsiK5u_PEXbweo_ijMzKo'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
