import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { CPU } from '../model/cpu';
import { Sort } from '../util/sort';

/**
 * This is a directive for sorting table columns
 */
@Directive({
  selector: '[appSortCol]'
})
export class SortColDirective {
  // naming this input the same as the directive name is what allows us to use the shortcut
  // i.e. <th [appSortCol]="cpu$">
  @Input() appSortCol: Observable<CPU[]> | undefined;

  // initialize the target element of the directive
  constructor(private targetElem: ElementRef) { }

  // this decorator binds the DOM element click event to this function
  @HostListener('click')
  sortData(){
    // subscribe to the observable
    this.appSortCol?.subscribe((cpus: CPU[]) => {
      // our sort logic
      const sort = new Sort();
      // the DOM element
      const element = this.targetElem.nativeElement;
      // the DOM element properties
      const order = element.getAttribute('data-order');
      const type = element.getAttribute('data-type');
      const property = element.getAttribute('data-name');

      cpus.sort(sort.startSort(property,order,type));

      // logic to switch between asc and desc
      if (order === 'desc') {
        element.setAttribute('data-order', 'asc');
        element.innerHTML = property + '&nbsp;&uarr;'; //arrow up
      } else {
        element.setAttribute('data-order', 'desc');
        element.innerHTML = property + '&nbsp;&darr;'; //arrow down
      }
    })
  }

}
