import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appPoint]'
})
export class PointDirective {

  // the cached element ref
  el:ElementRef;

   // initialize the target element of the directive
   constructor(private targetElem: ElementRef, private renderer: Renderer2) {
     this.el = this.targetElem.nativeElement;
   }

   // bind to the mouse over of the element
   @HostListener('mouseenter')
   mouseEnter(){
     // use the renderer to set the element style
     this.renderer.setStyle(this.el, 'color', 'red');
     this.renderer.setStyle(this.el, 'cursor', 'pointer');
   }

   // bind to the mouse out of the element
   @HostListener('mouseleave')
   mouseLeave(){
     // use the renderer to set the element style
     this.renderer.setStyle(this.el, 'color', 'black');
     this.renderer.setStyle(this.el, 'cursor', '');
   }




}
