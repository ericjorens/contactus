import { Component } from '@angular/core';
import { cpus } from './model/cpus';
import data from '../assets/cpus.json';
import { CPU } from './model/cpu';
import { Observable, of } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ContactUs';

  _cpus: CPU[] = cpus;

  //an array
  // _cpusJson: CPU[] = [];

  //observable
  cpu$: Observable<CPU[]>;
  _selectedCpu: CPU = {
    id: 1,
    name: 'Placeholder',
    desc: 'App Placeholder',
    cores: 0,
    speed: 0,
    bus: 0,
    aux: 0,
    lat: 0,
    lng: 0,
  };

  constructor(){
    this.cpu$ = this.getCpus();
  }

  private getCpus(): Observable<CPU[]> {
    return of(data);
  }

  setSelectedCpu(cpu:CPU){
    this._selectedCpu = cpu;
  }



}
