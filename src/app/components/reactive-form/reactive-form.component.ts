import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CPU } from 'src/app/model/cpu';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit, OnChanges {
  @Input() selectedCpu: CPU = {
    id: 1,
    name: 'Placeholder',
    desc: 'Reactive Form Placeholder',
    cores: 0,
    speed: 0,
    bus: 0,
    aux: 0,
    lat: 0,
    lng: 0,
  };

  //you can create the model manually
  cpuForm = new FormGroup({
    id:new FormControl('1'),
    name:new FormControl('Placeholder'),
    desc:new FormControl('this is a test'),
    cores:new FormControl('8'),
    speed:new FormControl('1.3'),
    aux:new FormControl('1'),
    bus:new FormControl('1'),
    lat:new FormControl('0'),
    lng:new FormControl('0')
  });

  // or use the service
  cpuForm2 = this.fb.group({
    id: ['1', Validators.required],
    name:['Test2'],
    desc:['test2'],
    cores:['6'],
    speed:['2'],
    aux:['1'],
    bus:['1'],
    lat:['1'],
    lng:['1'],
    properties: this.fb.array([
      this.fb.control('')
    ])
  });

  // get the props from our model
  get properties(){
    return this.cpuForm2.get('properties') as FormArray;
  }

  // add a prop to the model
  addProp(){
    this.properties.push(this.fb.control(''));
  }

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
  }

  //when our input changes we want to patch our form controls
  ngOnChanges(changes: SimpleChanges): void {
      this.cpuForm2.patchValue(changes.selectedCpu.currentValue);
  }

  // bound to the form submit
  onSubmit() {
    console.warn(this.cpuForm2.value);
  }

}
