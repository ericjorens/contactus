import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CpuSpotterComponent } from './cpu-spotter.component';

describe('CpuSpotterComponent', () => {
  let component: CpuSpotterComponent;
  let fixture: ComponentFixture<CpuSpotterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CpuSpotterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CpuSpotterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
