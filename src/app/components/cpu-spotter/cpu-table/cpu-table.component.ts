import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { CPU } from 'src/app/model/cpu';

@Component({
  selector: 'app-cpu-table',
  templateUrl: './cpu-table.component.html',
  styleUrls: ['./cpu-table.component.css'],
})
export class CpuTableComponent implements OnInit {

  // our component IO
  @Input() cpus?: Observable<CPU[]>;
  @Output() cpuClick = new EventEmitter<CPU>();

  constructor() {}

  ngOnInit(): void {}

  // this is the row click action
  rowClick(cpu: any, idx: number): any {
    // console.log(JSON.stringify(cpu));
    this.cpuClick.emit(cpu);
  }
}
