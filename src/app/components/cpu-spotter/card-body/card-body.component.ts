import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';

/**
 * This represents an card
 */
@Component({
  selector: 'app-card-body',
  templateUrl: './card-body.component.html',
  styleUrls: ['./card-body.component.css'],
  animations: [
    trigger('cardGrow', [
      state('default', style({
        transform: 'scale(1)'
      })),
      state('grown', style({
        transform: 'scale(1.05)'
      })),
      transition('default => grown', [
        animate('200ms')
      ]),
      transition('grown => default', [
        animate('100ms')
      ])
    ])
  ]
})
export class CardBodyComponent implements OnInit {
  // default our input to the card
  @Input() cardDetails: {title:string, desc:any} = {
    title: 'Placeholder',
    desc: ""
};

// animation toggles
  _cardState = "default";
  _toggle:boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  // card enter animation
  cardEntered() {
    this._cardState = "grown";
    this.toggleHighlight(true);
  }

  // card leave animation
  cardLeft(){
    this._cardState = "default";
    this.toggleHighlight(false);
  }

  // our highlight toggle
  toggleHighlight(val:boolean) {
    this._toggle = val;
  }

  // returns the border highlight color for this component
  // this could be made dynamic in the future
  getBorderStyle(): string{
    return this._toggle ? '0 3px 10px rgb(200 255 200)' : '';
  }

}
