import { Component, Input, OnInit } from '@angular/core';
import { CPU } from 'src/app/model/cpu';


/**
 * This represents our card row
 */
@Component({
  selector: 'app-card-pane',
  templateUrl: './card-pane.component.html',
  styleUrls: ['./card-pane.component.css'],
})
export class CardPaneComponent implements OnInit {
  @Input() cpu: CPU = {
    id: 1,
    name: 'Placeholder',
    desc: '2.4 GHz Giant Lizard',
    cores: 8,
    speed: 2.4,
    bus: 2,
    aux: 0,
    lat: 50,
    lng: 33.4,
  };

  constructor() {}

  ngOnInit(): void {}

  // This is our helper function but would be better suited for a util class
  getStringOf(n:number){
    if(!!n){
       return n.toString() || '';
    }
    return '';

  }
}
