import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPaneComponent } from './card-pane.component';

describe('CardPaneComponent', () => {
  let component: CardPaneComponent;
  let fixture: ComponentFixture<CardPaneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardPaneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
