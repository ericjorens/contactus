import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { Observable } from 'rxjs';
import { CPU } from 'src/app/model/cpu';

@Component({
  selector: 'app-cpu-spotter',
  templateUrl: './cpu-spotter.component.html',
  styleUrls: ['./cpu-spotter.component.css'],
})
export class CpuSpotterComponent implements OnInit, OnChanges {
  // our component IO
  @Input() cpus?: Observable<CPU[]>;
  @Output() cpuClick = new EventEmitter<CPU>();

  // create a placeholder object to appease the typing
  _selectedCpu: CPU = {
    id: 1,
    name: 'Placeholder',
    desc: 'Spotter Placeholder',
    cores: 0,
    speed: 0,
    bus: 0,
    aux: 0,
    lat: 0,
    lng: 0,
  };

  constructor() {}

  // clear the selected cpu when changes to the input
  ngOnChanges(changes: SimpleChanges) {
    this._selectedCpu = Object.assign({});
  }

  ngOnInit(): void {}

  // get the cached selected cpu
  getSelectedCpu(): any {
    return this._selectedCpu;
  }

  // allow our child components to select the cpu
  setCpu(cpu: any) {
    this._selectedCpu = cpu;
    this.cpuClick.emit(this._selectedCpu);
  }

  // return the lat of the selected cpu for map component
  getLat(): number {
    return this._selectedCpu.lat;
  }

  // return the lng of the selected cpu for the map component
  getLng(): number {
    return this._selectedCpu.lng;
  }
}
