import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-map-row',
  templateUrl: './map-row.component.html',
  styleUrls: ['./map-row.component.css'],
})
export class MapRowComponent implements OnInit, OnChanges {
  // our component IO
  @Input() lat = 51.678418;
  @Input() lng = 7.809007;

  constructor() {}

  // change detection for our inputs
  ngOnChanges(changes: SimpleChanges): void {
    if (!!changes) {
      this.lat = !!changes.lat ? changes.lat.currentValue : 0;
      this.lng = !!changes.lng ? changes.lng.currentValue : 0;
    }
  }

  ngOnInit(): void {}

  // center the map
  changePos() {
    this.lat = 0;
    this.lng = 0;
  }
}
