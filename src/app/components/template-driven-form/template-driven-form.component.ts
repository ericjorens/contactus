import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { CPU } from 'src/app/model/cpu';


@Component({
  selector: 'app-template-driven-form',
  templateUrl: './template-driven-form.component.html',
  styleUrls: ['./template-driven-form.component.css'],
})
export class TemplateDrivenFormComponent implements OnInit {
  @Input() selectedCpu: CPU = {
    id: 1,
    name: 'Placeholder',
    desc: 'Template Driven Form Placeholder',
    cores: 0,
    speed: 0,
    bus: 0,
    aux: 0,
    lat: 0,
    lng: 0,
  }

  constructor() {
  }

  ngOnInit(): void {}

}
